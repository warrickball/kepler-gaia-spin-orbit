import matplotlib.pyplot as pl
from astropy.table import Table

# plot figure 1

data = Table.read('data.ecsv')

pl.clf()
pl.plot([0, 180], [0, 180], 'k--')
pl.errorbar(data['inc'], data['i'], xerr=data['inc_error'], yerr=(data['e_i'], data['E_i']),
            fmt='o', label=r'$i_\mathrm{rot}$')
pl.errorbar(data['inc'], 180-data['i'], xerr=data['inc_error'], yerr=(data['E_i'], data['e_i']),
            fmt='o', label=r'180°$-i_\mathrm{rot}$')
I = data['inc'] > 90
pl.plot(data['inc'][I], data['i'][I], 'wo', ms=3, zorder=10)
pl.plot(data['inc'][~I], 180-data['i'][~I], 'wo', ms=3, zorder=10)

# manually add alternative data for KIC 7510397 from Appourchaux et al. (2015)
row = data[data['KIC']==7510397]
pl.errorbar([14], row['i'], xerr=[[10],[11]], yerr=[row['e_i'], row['E_i']], fmt='C0s')
pl.errorbar([14], 180-row['i'], xerr=[[10],[11]], yerr=[row['E_i'], row['e_i']], fmt='C1s')
pl.plot(14, 180-row['i'], 'ws', ms=3, zorder=10)

pl.xlim(0,180)
pl.xticks([0, 45, 90, 135, 180])
pl.ylim(0,180)
pl.yticks([0, 45, 90, 135, 180])
pl.xlabel(r'orbital inclination $i_\mathrm{orb}$ (${}^\circ$)')
pl.ylabel(r'rotational inclination $i_\mathrm{rot}$ (${}^\circ$)')
pl.legend()
pl.savefig('figure1.pdf')
