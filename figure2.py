#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as pl

def lower(sig):
    return lambda x: x-sig

def upper(sig):
    return lambda x: x+sig

def func(x, y):
    return 0.25*np.sin(x)*np.sin(y)

# diagram of how integration works

pl.clf()
sig = 20.0

# background contours of P(i_orb,i_rot)
x, y = np.mgrid[0:np.pi:401j,0:np.pi:401j]
# pl.contourf(np.degrees(x), np.degrees(y), func(x, y), levels=50, vmin=0, vmax=0.25, cmap='Purples')
pl.imshow(func(x, y), extent=(0,180,0,180), cmap='Blues', vmax=1/3)

# translucent bands of the X
# kwargs = {'fc': 'C1', 'ec': None, 'alpha': 0.2}
# x = np.array([-10., 90.-sig])
# pl.fill_between(x, lower(sig)(x), upper(sig)(x), **kwargs)
# pl.fill_between(x, 180-lower(sig)(x), 180-upper(sig)(x), **kwargs)
# pl.fill_between(180-x, 180-lower(sig)(x), 180-upper(sig)(x), **kwargs)
# pl.fill_between(180-x, lower(sig)(x), upper(sig)(x), **kwargs)
# x = np.array([90.-sig, 90.])
# pl.fill_between(x, lower(sig)(x), 180-lower(sig)(x), **kwargs)
# pl.fill_between(180-x, lower(sig)(x), 180-lower(sig)(x), **kwargs)

# shade the regions that *aren't* integrated
kwargs = {'fc': 'k', 'ec': None, 'alpha': 0.2}
pl.fill_between([sig, 90, 180-sig], [0,0,0], [0, 90-sig, 0], **kwargs)
pl.fill_between([sig, 90, 180-sig], [180,180,180], [180, 90+sig, 180], **kwargs)
pl.fill_betweenx([sig, 90, 180-sig], [0,0,0], [0, 90-sig, 0], **kwargs)
pl.fill_betweenx([sig, 90, 180-sig], [180,180,180], [180, 90+sig, 180], **kwargs)

# edges of the X
# pl.plot([sig, 180], [0, 180-sig], 'k--')
# pl.plot([0, 180-sig], [sig, 180], 'k--')
pl.plot([sig, 90], [0, 90-sig], 'k--')
pl.plot([90+sig, 180], [90, 180-sig], 'k--')
pl.plot([0, 90-sig], [sig, 90], 'k--')
pl.plot([90, 180-sig], [90+sig, 180], 'k--')
pl.plot([0, 90-sig], [180-sig, 90], 'k--')
pl.plot([sig, 90], [180, 90+sig], 'k--')
pl.plot([90, 180-sig], [90-sig, 0], 'k--')
pl.plot([90+sig, 180], [90, sig], 'k--')

# details
kwargs = {'c': 'k', 'fontsize': 18, 'ha': 'left'}
pl.text(5, 5, r"$i_\mathrm{rot}=i_\mathrm{orb}\pm\sigma$", rotation=45, va='bottom', **kwargs)
pl.text(5, 175, r"$i_\mathrm{rot}=180^\circ-i_\mathrm{orb}\pm\sigma$", rotation=-45, va='top', **kwargs)
pl.xlim(0,180)
pl.xticks([0, 45, 90, 135, 180])
pl.ylim(0,180)
pl.yticks([0, 45, 90, 135, 180])
pl.xlabel(r'orbital inclination $i_\mathrm{orb}$ (${}^\circ$)')
pl.ylabel(r'rotational inclination $i_\mathrm{rot}$ (${}^\circ$)')
pl.gca().set_aspect('equal')
pl.savefig('figure2-domain.pdf')


# integration

from scipy.integrate import dblquad

def step_really(sig):
    # unambiguous
    l = lower(sig)
    u = upper(sig)

    yeah = 0.0
    yeah += dblquad(func, 0, sig, 0, u)[0]
    yeah += dblquad(func, sig, np.pi/2, l, u)[0]
    yeah *= 2
    return yeah

def step_maybe(sig):
    # with ambiguity
    l = lower(sig)
    u = upper(sig)

    hmmm = 0.0
    hmmm += dblquad(func, 0, sig, 0, u)[0]
    hmmm += dblquad(func, sig, np.pi/2-sig, l, u)[0]
    hmmm += dblquad(func, np.pi/2-sig, np.pi/2, l, np.pi/2)[0]
    hmmm *= 4
    return hmmm

# Monte Carlo check of integrations
N = 1_000_000
rot, orb = np.arccos(np.random.uniform(low=-1, high=1, size=(2,N)))
MC = sum((np.abs(rot-orb) < np.radians(sig)))/N
np.testing.assert_almost_equal(MC, step_really(np.radians(sig)), decimal=3)
MC = sum((np.abs(rot-orb) < np.radians(sig)) | (np.abs(np.pi-rot-orb) < np.radians(sig)))/N
np.testing.assert_almost_equal(MC, step_maybe(np.radians(sig)), decimal=3)
# print(sum((np.abs(rot-orb) < np.radians(sig)) | (np.abs(np.pi-rot-orb) < np.radians(sig)))/N)

sigs = np.linspace(0.5,60., 101)
hmmm = np.squeeze([step_maybe(np.radians(sig)) for sig in sigs])
yeah = np.squeeze([step_really(np.radians(sig)) for sig in sigs])

pl.clf()
for i, N in enumerate([1,5,25]):
    pl.plot(sigs, hmmm**N, 'C%i-' % i, label=N)
    pl.plot(sigs, yeah**N, 'C%i--' % i)

# pl.plot(sigs, hmmm**5, 'C1-', label="5")
# pl.plot(sigs, yeah**5, 'C1--')

# pl.plot(sigs, hmmm**25, 'C2-', label="25")
# pl.plot(sigs, yeah**25, 'C2--')

pl.plot(np.nan, np.nan, 'k-', label=r"$i_\mathrm{rot}=i_\mathrm{orb}\ \mathrm{or}\ 180^\circ-i_\mathrm{orb}$")
pl.plot(np.nan, np.nan, 'k--', label=r"$i_\mathrm{rot}=i_\mathrm{orb}\ \mathrm{only}$")

pl.ylabel('probability');
pl.xlabel('$\sigma$ (degrees)');

pl.xlim(0, sigs.max())
pl.ylim(1e-3, 1)
pl.yscale('log')
pl.legend(ncol=2, frameon=True, loc='lower left')
# pl.gca().set_aspect('equal')

kwargs = {'c': '#d0d0d0', 'lw': 1.0, 'zorder': -10}
pl.axvline(20, **kwargs)
pl.axhline(step_maybe(np.radians(20))**5, **kwargs)

pl.savefig('figure2.pdf')
