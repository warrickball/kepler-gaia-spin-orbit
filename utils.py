import numpy as np

def thiele_innes_to_campbell(A, B, F, G):
    """Converts Thiele–Innes elements A, B, F and G into Campbell elements
    a, i, Ω and ω, largely following Appendix A of Halbwachs et al. (2022):

    https://ui.adsabs.harvard.edu/abs/2022arXiv220605726H

    Inverse of ``campbell_to_thiele_innes``.
    """
    # omega probably incorrect if > π
    u = (A*A + B*B + F*F + G*G)/2
    v = A*G-B*F
    a = (u+((u-v)*(u+v))**0.5)**0.5

    omega_plus_Omega = np.arctan2(B-F, A+G)
    omega_minus_Omega = np.arctan2(-B-F, A-G) # do sign tricks get correct quadrant?

    omega = (omega_plus_Omega + omega_minus_Omega)/2
    Omega = (omega_plus_Omega - omega_minus_Omega)/2

    d1 = np.abs((A+G)*np.cos(omega_minus_Omega))
    d2 = np.abs((F-B)*np.sin(omega_minus_Omega))

    if isinstance(d1, float):
        if d1 >= d2:
            inc = 2*np.arctan(np.sqrt(np.abs((A-G)*np.cos(omega+Omega))/d1))
        else:
            inc = 2*np.arctan(np.sqrt(np.abs((B+F)*np.sin(omega+Omega))/d2))
    else:
        inc = 2*np.arctan(np.sqrt(np.abs((A-G)*np.cos(omega+Omega))/d1))
        inc[d1 < d2] = (2*np.arctan(np.sqrt(np.abs((B+F)*np.sin(omega+Omega))/d2)))[d1 < d2]
    
    return a, inc, Omega, omega


def campbell_to_thiele_innes(a, inc, Omega, omega):
    """Converts Campbell elements a, i, Ω and ω into Thiele–Innes elements
    A, B, F and G.  Inverse of ``thiele_innes_to_campbell``.
    """
    A = a*(np.cos(Omega)*np.cos(omega) - np.sin(Omega)*np.sin(omega)*np.cos(inc))
    B = a*(np.sin(Omega)*np.cos(omega) + np.cos(Omega)*np.sin(omega)*np.cos(inc))
    F = a*(-np.cos(Omega)*np.sin(omega) - np.sin(Omega)*np.cos(omega)*np.cos(inc))
    G = a*(-np.sin(Omega)*np.sin(omega) + np.cos(Omega)*np.cos(omega)*np.cos(inc))

    return A, B, F, G


def mass_functions(A, B, F, G, C, H, plx, P):
    """Returns astrometric and spectroscopic mass functions as defined
    by equations (2) and (4) of Arenou et al. (2022):

    https://ui.adsabs.harvard.edu/abs/2022arXiv220605595G
    """
    a, inc, Omega, omega = thiele_innes_to_campbell(A, B, F, G)
    astro = (a/plx)**3/P**2 # eq. (2), also eq. (13) of Halbwachs et al. (2022)
    spectro = (C**2+H**2)**1.5/P**2 # eq. (4)
    return astro, spectro


def matrix_to_corr_vec(M):
    """Map upper triangle into vector like ``corr_vec```.

    Inverse of ``corr_vec_to_matrix``."""
    n = len(M)
    # v = [M[0,1], M[0,2], M[1,2], ...]
    v = np.array([M[j][i] for i in range(1,n) for j in range(i)])
    return v


def corr_vec_to_matrix(v):
    """Map vector like ``corr_vec`` into upper triangle of matrix and symmetrize.

    Inverse of ``matrix_to_corr_vec``."""
    m = len(v)
    n = int((1+np.sqrt(1+8*m))/2)
    assert m == n*(n-1)//2
    M = np.eye(n)
    k = 0
    for i in range(1,n):
        for j in range(i):
            M[j,i] = M[i,j] = v[k]
            k += 1
            
    return M


def rebuild_covariance(row):
    """Given a row of ``nss_two_body_orbit``, reconstruct the covariance matrix.

    https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_non--single_stars_tables/ssec_dm_nss_two_body_orbit.html#nss_two_body_orbit-bit_index

    Returns parameter names and covariance matrix."""

    # long if-elif-else ladder to determine parameters
    if row['nss_solution_type'] == 'Orbital':
        if row['bit_index'] == 8179:
            params = ['ra', 'dec', 'parallax', 'pmra', 'pmdec',
                      'a_thiele_innes', 'b_thiele_innes',
                      'f_thiele_innes', 'period', 't_periastron']
        elif row['bit_index'] == 8191:
            params = ['ra', 'dec', 'parallax', 'pmra', 'pmdec',
                      'a_thiele_innes', 'b_thiele_innes',
                      'f_thiele_innes', 'g_thiele_innes',
                      'eccentricity', 'period', 't_periastron']
        else:
            raise ValueError('bit_index should be 8179 or 8191, not %i' % row['bit_index'])

    elif row['nss_solution_type'] == 'SB1':
        assert row['bit_index'] == 127
        params = ['period', 'center_of_mass_velocity',
                  'semi_amplitude_primary', 'eccentricity', 'arg_periastron',
                  't_periastron']

    elif row['nss_solution_type'] == 'AstroSpectroSB1':
        if row['bit_index'] == 65435:
            params = ['ra', 'dec', 'parallax', 'pmra', 'pmdec',
                      'a_thiele_innes', 'b_thiele_innes', 'f_thiele_innes',
                      'h_thiele_innes',
                      'center_of_mass_velocity', 'period', 't_periastron']
        elif row['bit_index'] == 65535:
            params = ['ra', 'dec', 'parallax', 'pmra', 'pmdec',
                      'a_thiele_innes', 'b_thiele_innes', 'f_thiele_innes',
                      'g_thiele_innes', 'c_thiele_innes', 'h_thiele_innes',
                      'center_of_mass_velocity', 'eccentricity', 'period', 't_periastron']
        else:
            raise ValueError('bit_index should be 65436 or 65535, not %i' % row['bit_index'])

    else:
        raise ValueError("can't yet handle nss_solution_type %s" % row['nss_solution_type'])

    corr = corr_vec_to_matrix(row['corr_vec'])

    vals = np.array([row[k] for k in params])
    std = np.array([row[k+'_error'] for k in params])
    cov = corr*std[:,None]*std[None,:]
    return params, vals, cov
