#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as pl
from scipy.optimize import fsolve
from astroquery.gaia import Gaia
from astroquery.vizier import Vizier
from astropy.io import fits
from astropy import table
import utils

# Kepler-Gaia cross-match from gaia-kepler.fun by Megan Bedell
# https://www.dropbox.com/s/pk5cgwjxanczn6b/kepler_dr3_good.fits?dl=0
try:
    b = fits.getdata('kepler_dr3_good.fits')
except FileNotFoundError:
    print("Please download Megan Bedell's cross-match from "
          "https://www.dropbox.com/s/pk5cgwjxanczn6b/kepler_dr3_good.fits?dl=0")
    exit(1)

# fetch data for Hall et al. (2021)
v = Vizier(columns=['**'])
v.ROW_LIMIT = -1
h = v.get_catalogs('J/other/NatAs/5.707')[0]

# start building the data table, `data`
# these columns are copied from the DR3 data
copy = ['period', 'period_error', 'eccentricity', 'eccentricity_error',
        'goodness_of_fit', 'significance'] # copy from DR3 data
dtype = [[(k, float), (k+'_error', float)] for k in
         ['plx', 'a', 'inc', 'Omega', 'omega', 'M2', 'a_over_plx']]
dtype = sum(dtype, start=[]) + [(k, float) for k in copy] + [('nss_type', 'U20')]
data = np.full(len(h), np.nan, dtype=dtype)

# define a utility for storing properties in `data`
def stats(k, x):
    global i, data
    data[i][k] = np.mean(x)
    data[i][k+'_error'] = np.std(x)

# loop over stars in Hall et al. sample
for i, hi in enumerate(h):

    # find match in Bedell cross-match; if none, move on
    bi = b[b['kepid']==hi['KIC']]
    if len(bi) == 0:
        continue

    # if the star is single, move on
    nss = bi['non_single_star'][0]
    if nss == 0:
        continue
    
    # query Gaia for entry in nss_two_body_orbit table
    try:
        job = Gaia.launch_job("""
            SELECT TOP 100 *
            FROM gaiadr3.nss_two_body_orbit
            WHERE gaiadr3.nss_two_body_orbit.source_id = %i""" % bi['source_id'])
        row = job.get_results()[0]
    except:
        data['period'][i] = -nss
        print("WARNING: KIC %i has nss=%i but nss_two_body_orbit query failed; probably only a RV trend" % (hi['KIC'], nss))
        continue

    # copy some data
    for k in copy:
        data[i][k] = row[k]

    data[i]['nss_type'] = row['nss_solution_type']

    # convert periods from days to years
    data['period'][i] /= 365.25
    data['period_error'][i] /= 365.25

    # if not an astrometric binary, move on
    if nss != 1 and nss != 3:
        continue

    params, mean, cov = utils.rebuild_covariance(row) # unpack covariances

    # propagate uncertainties by Monte-Carlo simulation
    samples = np.random.multivariate_normal(mean, cov, size=100_000)
    A, B, F, G = [samples[:,params.index(k+'_thiele_innes')] for k in 'abfg']
    a, inc, Omega, omega = utils.thiele_innes_to_campbell(A, B, F, G)
    plx, P, eccentricity = [samples[:,params.index(k)] for k in ['parallax', 'period', 'eccentricity']]
    P /= 365.25
    C = H = 0.0
    astro, spectro = utils.mass_functions(A, B, F, G, C, H, plx, P)
    M2 = fsolve(lambda M2: M2**3/(M2+hi['Mass'])**2-np.mean(astro), 0.001, full_output=True)[0][0]

    # store results
    data[i]['M2'] = M2

    stats('eccentricity', eccentricity)
    stats('plx', plx)
    stats('period', P)
    stats('a', a)
    stats('inc', np.degrees(inc))
    stats('Omega', np.degrees(Omega))
    stats('omega', np.degrees(omega))
    stats('a_over_plx', a/plx)

# combine data derived from Gaia with data from Hall et al.
# and drop rows with no period
I = np.where(~np.isnan(data['period']))[0]
data = table.hstack([h[I], table.Table(data[I])])
data.remove_columns(['recno', 'Index'])

data.write('data.ecsv')
print(data)
