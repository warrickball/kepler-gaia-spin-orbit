# Kepler Gaia Spin Orbit

This repository contains Python code to recreate Figs 1 and 2 and the data for Table 1 of

> Ball, W. H., Triaud, A. H. M. J., Hatt, E., Nielsen, M. B. & Chaplin, W. J. 2023.
> *Projected spin-orbit alignments from Kepler asteroseismology and Gaia astrometry*, accepted in MNRASL

You'll need to download the one-to-one Kepler-Gaia [cross-match by
Megan Bedell](https://gaia-kepler.fun) with e.g.

    wget https://www.dropbox.com/s/pk5cgwjxanczn6b/kepler_dr3_good.fits?dl=0 -O kepler_dr3_good.fits

The script `create_data.py` will retrieve the data
from [Hall et al. (2021)](https://ui.adsabs.harvard.edu/abs/2021NatAs...5..707H),
use the cross-match to look up the orbital parameters
in the relevant Gaia table (`nss_two_body_orbit`), arrange the combined data
into a table and save it to `data.ecsv`.
The saved table can be read back in with e.g.
```python
from astropy.table import Table

data = Table.read('data.ecsv')
```

The script `figure1.py` will then create Figure 1 from the paper and save it to `figure1.pdf`.

The script `figure2.py` will create Figure 2 from the paper and save it to `figure2.pdf`.
The script will also create a diagram showing the region of integration used
to create Figure 2 and save it to `figure2-domain.pdf`.
(This diagram was included in an earlier revision of the article.)
These figures don't depend on the data.

`utils.py` contains a few relevant functions for
working with the Gaia binary data.

The script requires 
[NumPy](https://numpy.org/),
[SciPy](https://scipy.org/),
[Matplotlib](https://matplotlib.org/),
[Astropy](https://www.astropy.org/) and
[Astroquery](https://astroquery.readthedocs.io/).
